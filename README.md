> Este proyecto está basado en  [RasaHQ/chatroom](https://github.com/RasaHQ/chatroom)

## Tecnologías necesarias
`React` `JavaScript` `GitLab` `GitLab CI/CD` `SonarCloud` `Slack` `Heroku`


## Estado del código

 GitLab CI/CD | SonarCloud  
 -- | --
 [![pipeline status](https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-widget/badges/develop/pipeline.svg)](https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-widget/-/commits/develop) | [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=daxamayac.upm_chatbot-widget&metric=alert_status)](https://sonarcloud.io/dashboard?id=daxamayac.upm_chatbot-widget)


Compatibilidad Rasa 2.6.x


### Más documentación en

[Docs](https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-docs)

### License
AGPL v3
Made by [scalable minds](https://scalableminds.com)
