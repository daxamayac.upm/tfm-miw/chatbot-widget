// @flow

import "unfetch/polyfill";
import "@babel/polyfill";
import React from "react";
import ReactDOM from "react-dom";
import { uuidv4 } from "app/shared/utils";

import DebuggerView from "app/debug/DebuggerView";

const USERID_STORAGE_KEY = "simple-chatroom-cid";

window.DebugChatroom = function(options: ChatroomOptions) {
  let sessionUserId = window.sessionStorage.getItem(USERID_STORAGE_KEY);

  const isNewSession = sessionUserId == null;

  if (isNewSession) {
    sessionUserId = uuidv4();
    window.sessionStorage.setItem(USERID_STORAGE_KEY, sessionUserId);
  }

  this.ref = ReactDOM.render(
    <DebuggerView
      rasaToken={options.rasaToken}
      userId={sessionUserId}
      host={options.host}
      title={options.title || "Chat"}
      speechRecognition={options.speechRecognition}
      welcomeMessage={options.welcomeMessage}
      waitingTimeout={options.waitingTimeout}
      fetchOptions={options.fetchOptions}
      voiceLang={options.voiceLang}
    />,
    options.container
  );

  const { startMessage } = options;
  if (isNewSession && startMessage != null) {
    this.ref.getChatroom().sendMessage(startMessage);
  }
};
