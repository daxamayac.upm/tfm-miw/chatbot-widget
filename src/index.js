// @flow
import "unfetch/polyfill";
import "@babel/polyfill";
import React from "react";
import ReactDOM from "react-dom";

import {uuidv4} from "app/shared/utils";
import ConnectedChatroom from "app/ConnectedChatroom";
import {ChannelType} from "app/models/ChannelType";

const USERID_STORAGE_KEY = "simple-chatroom-cid";

type ChatroomOptions = {
    host: string,
    title?: string,
    welcomeMessage?: string,
    startMessage?: string,
    container: HTMLElement,
    waitingTimeout?: number,
    fetchOptions?: RequestOptions,
    rasaToken?: string,
};

window.Chatroom = function (options: ChatroomOptions) {
    let sessionUserId = window.sessionStorage.getItem(USERID_STORAGE_KEY);

    const isNewSession = sessionUserId == null;

    if (isNewSession) {
        sessionUserId = uuidv4();
        window.sessionStorage.setItem(USERID_STORAGE_KEY, sessionUserId);

    }
    if (!localStorage.getItem("notifyBy"))
        localStorage.setItem("notifyBy", ChannelType.RASA)
    localStorage.setItem("host", options.host)
    this.ref = ReactDOM.render(
        <ConnectedChatroom
            userId={sessionUserId}
            host={options.host}
            title={options.title || "Chat"}
            welcomeMessage={options.welcomeMessage}
            waitingTimeout={options.waitingTimeout}
            fetchOptions={options.fetchOptions}
        />,
        options.container
    );

    this.openChat = () => {
        this.ref.setState({isOpen: true});
    };

    this.closeChat = () => {
        this.ref.setState({isOpen: false});
    };

    if (isNewSession && options.startMessage != null) {
        this.ref.sendMessage(options.startMessage);
    }
};
