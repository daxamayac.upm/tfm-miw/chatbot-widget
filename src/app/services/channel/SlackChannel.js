import type {Channel} from "app/services/channel/Channel";
import {WebClient} from "@slack/web-api";
import EventEmitter from "app/shared/EventEmitter";

export class SlackChannel implements Channel {

    slackWebApi: WebClient
    socketClient: WebSocket

    constructor() {
        if(typeof SlackChannel.instance==="object"){
            return SlackChannel.instance
        }

        if (typeof this.slackWebApi == 'undefined' && typeof this.socketClient == 'undefined') {
            this.slackWebApi = this._connectSlackWebApi("xoxb-" + localStorage.getItem('token'));
            this._getWss().then(wss => this._connectWss(wss));
        }
        SlackChannel.instance = this;
        return this;
    }

    _connectSlackWebApi(token) {
        return new WebClient(token)
    }

    _connectWss(wss) {
        try {
            this.socketClient = new WebSocket(wss);
            this.socketClient.onopen = function (e) {
                console.log("ON_OPEN")
                console.log(e)
            }
            this._listenerSocket()
        } catch (error) {
            console.log("NO_OPEN")
            console.error("Error Socket", error)
        }
    }

    async sendMessage(messageText, username) {
        let channel = localStorage.getItem('channel')

        return this.slackWebApi.chat.postMessage({
            channel: channel,
            text: messageText,
            username: username || ''
        })
    }


    _listenerSocket() {
        console.log("_listener")
        let socketClient = this.socketClient
        let getWss = () => this._getWss()
        let connect = (wss) => this._connectWss(wss)
        socketClient.onmessage = function (event) {
            console.log("ON_MESSAGE");

            if (event.data === "UNAUTHENTICATED: cache_error") {
                console.log("error socket")
                getWss().then(wss => connect(wss)).catch(error => console.log(error));
                return;
            }

            let data = JSON.parse(event.data)
            let envelope_id = data.envelope_id

            if (envelope_id) {
                let channel = localStorage.getItem("channel");
                let responseAcknowledge = JSON.stringify({envelope_id: envelope_id})
                let message = data.payload.event

                if (message.type === "message" && typeof message.subtype === 'undefined' && message.channel === channel) {

                   EventEmitter.emit("newSlackMessage", message)
                }
                socketClient.send(responseAcknowledge)
            }
        }

        socketClient.onclose = function (event) {
            console.log("ON_CLOSE")
            console.log(event)
        };

        socketClient.onerror = function (error) {
            console.log("ON_ERROR")
            console.error(error)
        };
    }

    async _getWss() {
        let message = {
            sender: sessionStorage.getItem("simple-chatroom-cid"),
            message: "/trigger_handoff_ws"
        }

        const fetchOptions = Object.assign({}, {
            method: "POST",
            body: JSON.stringify(message),
            headers: {
                "Content-Type": "application/json"
            }
        })
        let host = localStorage.getItem("host") ||""

        const response = await fetch(
            host,
            fetchOptions
        );
        const data = await response.json();
        return data[0].custom.wss
    }
}

