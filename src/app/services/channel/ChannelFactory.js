
import type {Channel} from "app/services/channel/Channel";
import {ChannelType} from "app/models/ChannelType";
import {SlackChannel} from "app/services/channel/SlackChannel";
import {RasaChannel} from "app/services/channel/RasaChannel";

export class ChannelFactory {
    get(channelType: string): Channel {
        switch (channelType) {
            case ChannelType.SLACK:
                return new SlackChannel();
            case ChannelType.RASA:
                return new RasaChannel();
            default: {
                return null;
            }
        }
    }
}
