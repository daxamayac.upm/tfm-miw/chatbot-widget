import EventEmitter from "app/shared/EventEmitter";
import {createNewRasaMessage} from "app/models/RasaMessage";
import type {Channel} from "app/services/channel/Channel";

export class RasaChannel implements Channel {

    async sendMessage(messageText) {
        const rasaMessage = createNewRasaMessage(this._getUserId(), messageText);

        const fetchOptions = Object.assign({}, {
            method: "POST",
            body: JSON.stringify(rasaMessage),
            headers: {
                "Content-Type": "application/json"
            }
        })//, this.props.fetchOptions);
        let host = localStorage.getItem("host") || ""
        const response = await fetch(
            host,
            fetchOptions
        );
        const messages = await response.json();
        EventEmitter.emit("newRasaMessage", messages)
    }


    _getUserId() {
        return sessionStorage.getItem('simple-chatroom-cid')
    }
}
