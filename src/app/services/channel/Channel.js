export interface Channel {
    sendMessage(messageTex: string, channelId?: string, username?: string);
}
