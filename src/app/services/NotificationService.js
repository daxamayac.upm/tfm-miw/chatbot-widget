import {ChannelFactory} from "app/services/channel/ChannelFactory";
import {ChannelType} from "app/models/ChannelType";

export class NotificationService {

    channelFactory = new ChannelFactory()

    async notify(messageText) {
        let channelNotify = this.channelFactory.get(this._getNotifyBy())
        await channelNotify.sendMessage(messageText, 'Vixi');
    }

    startSlackHandoff(data) {
        localStorage.setItem("channel", data.channel)
        localStorage.setItem("token", data.token)
        localStorage.setItem("notifyBy", ChannelType.SLACK)

    }

    endSlackHandoff() {
        localStorage.removeItem("wss")
        localStorage.removeItem("channel")
        localStorage.removeItem("token")
        localStorage.setItem("notifyBy", ChannelType.RASA)
    }

    _getNotifyBy() {
        return localStorage.getItem('notifyBy')
    }
}

const instance = new NotificationService();

export default instance;
