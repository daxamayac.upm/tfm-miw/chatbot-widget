// @flow
import React, {Component} from "react";
import Chatroom from "app/Chatroom";
import {uuidv4} from "app/shared/utils";
import {ChatMessage, createNewUserChatMessage} from "app/models/ChatMessage";
import {RasaMessage, rasaMessageToBotChatMessage} from "app/models/RasaMessage";
import notificationService from "app/services/NotificationService";
import EventEmitter from "app/shared/EventEmitter";
import type {SlackMessage} from "app/models/SlackMessage";
import {slackMessageToBotChatMessage} from "app/models/SlackMessage";


type ConnectedChatroomProps = {
    userId: string,
    host: string,
    welcomeMessage: ?string,
    title: string,
    waitingTimeout: number,
    messageBlacklist: Array<string>,
    handoffIntent: string,
    fetchOptions?: RequestOptions,
};

type ConnectedChatroomState = {
    messages: Array<ChatMessage>,
    messageQueue: Array<ChatMessage>,
    isOpen: boolean,
    waitingForBotResponse: boolean,
    currentHost: string,
    currentTitle: string
};


export default class ConnectedChatroom extends Component<ConnectedChatroomProps,
    ConnectedChatroomState> {

    static defaultProps = {
        waitingTimeout: 5000,
        messageBlacklist: ["_restart", "_start", "/restart", "/start",
            "/handoff", "_handoff", "/trigger_handoff_wss", "_trigger_handoff_wss"],
        handoffIntent: "handoff"
    };

    state = {
        messages: [],
        messageQueue: [],
        isOpen: false,
        waitingForBotResponse: false,
        currentHost: `${this.props.host}`,
        currentTitle: `${this.props.title}`
    };

    handoffPayload = `\\/(${this.props.handoffIntent})\\b.*`;
    handoffRegex = new RegExp(this.handoffPayload);
    waitingForBotResponseTimer: ?TimeoutID = null;
    messageQueueInterval: ?IntervalID = null;
    chatroomRef = React.createRef<Chatroom>();

    componentDidMount() {
        const messageDelay = 800; //delay between message in ms
        this.messageQueueInterval = window.setInterval(
            this.queuedMessagesInterval,
            messageDelay
        );

        if (this.props.welcomeMessage) {
            const welcomeMessage = {
                message: {type: "text", text: this.props.welcomeMessage},
                time: Date.now(),
                username: "bot",
                uuid: uuidv4()
            };
            this.setState({messages: [welcomeMessage]});
        }
        EventEmitter.on("newRasaMessage", data => this.parseRasaMessage(data))
        EventEmitter.on("newSlackMessage", data => this.parseSlackMessage(data))
    }

    componentWillUnmount() {
        if (this.waitingForBotResponseTimer != null) {
            window.clearTimeout(this.waitingForBotResponseTimer);
            this.waitingForBotResponseTimer = null;
        }
        if (this.messageQueueInterval != null) {
            window.clearInterval(this.messageQueueInterval);
            this.messageQueueInterval = null;
        }
    }

    sendMessage = async (messageText: string) => {
        if (messageText === "") return;

        const userChatMessage = createNewUserChatMessage(this.props.userId, messageText);

        if (!this.props.messageBlacklist.includes(messageText) && !messageText.match(this.handoffRegex)
            && !messageText.includes("request_")) {
            this.setState({
                // Reveal all queued bot messages when the user sends a new message
                messages: [
                    ...this.state.messages,
                    ...this.state.messageQueue,
                    userChatMessage
                ],
                messageQueue: []
            });
        }

        this.setState({waitingForBotResponse: true});
        if (this.waitingForBotResponseTimer != null) {
            window.clearTimeout(this.waitingForBotResponseTimer);
        }
        this.waitingForBotResponseTimer = setTimeout(() => {
            this.setState({waitingForBotResponse: false});
        }, this.props.waitingTimeout);

        notificationService.notify(messageText).then().catch(e => console.log(e))

        if (window.ga != null) {
            window.ga("send", "event", "chat", "chat-message-sent");
        }
    };


    parseSlackMessage(slackMessage: SlackMessage) {
        let expandedMessages = [];
        let message = slackMessageToBotChatMessage(slackMessage);

        if (slackMessage.text === "_end") {
            notificationService.endSlackHandoff()
            return;
        }

        if (message) {
            expandedMessages.push(message);
        }

        this.parseMessages(expandedMessages)
    }


    parseRasaMessage(rasaMessages: Array<RasaMessage>) {
        const validMessageTypes = ["text", "image", "buttons", "attachment", "custom"];

        let expandedMessages = [];

        rasaMessages.filter((rasaMessage: RasaMessage) =>
            validMessageTypes.some(type => type in rasaMessage)
        ).forEach(rasaMessage => {
            let validMessage = false;
            let message = rasaMessageToBotChatMessage(rasaMessage);

            if (message) {
                expandedMessages.push(message);
                validMessage = true;
            }

            if (rasaMessage.custom && rasaMessage.custom.channel) {
                notificationService.startSlackHandoff(rasaMessage.custom)

                if (rasaMessage.custom.title) {
                    this.setState({
                        currentTitle: rasaMessage.custom.title
                    })
                }
                notificationService.notify("Auto>> los próximos mensajes que recibas serán de un cliente")
                    .then()
                    .catch(error => console.error("Error change to advisor", error))
                return;
            }

            if (validMessage === false) {
                console.error("Message unidentified", message)
                throw Error("Could not parse message from Bot or empty message");
            }

        });
        this.parseMessages(expandedMessages)
    }

    parseMessages(expandedMessages) {
        // Bot messages should be displayed in a queued manner. Not all at once
        const messageQueue = [...this.state.messageQueue, ...expandedMessages];
        this.setState({
            messageQueue,
            waitingForBotResponse: messageQueue.length > 0
        });
    }

    queuedMessagesInterval = () => {
        const {messages, messageQueue} = this.state;

        if (messageQueue.length > 0) {
            const message = messageQueue.shift();
            const waitingForBotResponse = messageQueue.length > 0;

            this.setState({
                messages: [...messages, message],
                messageQueue,
                waitingForBotResponse
            });
        }
    };

    handleButtonClick = (buttonTitle: string, payload: string) => {
        this.sendMessage(payload);
        if (window.ga != null) {
            window.ga("send", "event", "chat", "chat-button-click");
        }
    };

    handleToggleChat = () => {
        if (window.ga != null) {
            if (this.state.isOpen) {
                window.ga("send", "event", "chat", "chat-close");
            } else {
                window.ga("send", "event", "chat", "chat-open");
            }
        }
        this.setState({isOpen: !this.state.isOpen});
    };

    render() {
        const {messages, waitingForBotResponse} = this.state;

        const renderableMessages = messages
            .filter(
                message =>
                    message.message.type !== "text" || (
                    !this.props.messageBlacklist.includes(message.message.text) &&
                    !message.message.text.match(this.handoffRegex)) &&
                    !message.message.text.includes("request_")
            )
            .sort((a, b) => a.time - b.time);

        return (
            <Chatroom
                messages={renderableMessages}
                title={this.state.currentTitle}
                waitingForBotResponse={waitingForBotResponse}
                isOpen={this.state.isOpen}
                onToggleChat={this.handleToggleChat}
                onButtonClick={this.handleButtonClick}
                onSendMessage={this.sendMessage}
                ref={this.chatroomRef}
                host={this.state.currentHost}
            />
        );
    }
}
