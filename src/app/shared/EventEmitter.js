class EventEmitter {
    events: {};

    constructor() {
        if (typeof EventEmitter.instance === "object") {
            return EventEmitter.instance
        }
        this.events = {};
        EventEmitter.instance = this;
        return this;
    }

    _getEventListByName(eventName) {
        if (typeof this.events[eventName] === 'undefined') {
            this.events[eventName] = new Set();
        }
        return this.events[eventName];
    }

    on(eventName, fn) {
        this._getEventListByName(eventName).add(fn);
    }

    emit(eventName, ...args) {
        this._getEventListByName(eventName).forEach(
            function (fn) {
                fn.apply(this, args);
            }.bind(this)
        );
    }

}

const instance = new EventEmitter()
export default instance;
