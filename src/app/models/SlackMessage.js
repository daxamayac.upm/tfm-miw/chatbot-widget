import {createNewBotChatMessage} from "app/models/ChatMessage";

export type SlackMessage =
    | {| ts: string, text: string, username: string, bot_id: string, channel: string |}
    | {| ts: string, text: string, user: string, channel: string |}


export function createNewSlackMessage(channel: string, messageText: string, username: string): SlackMessage {
    return {
        channel: channel,
        text: messageText,
        username: username || 'Vixi'
    };
}

export function slackMessageToBotChatMessage(message: SlackMessage) {
    if (message.text) {
        return createNewBotChatMessage({type: "text", text: message.text});
    }

    return null;
}
