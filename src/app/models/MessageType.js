export type MessageType =
    | {
    type: "text",
    text: string
}
    | { type: "image", image: string }
    | {
    type: "button",
    buttons: Array<{ payload: string, title: string, selected?: boolean }>
}
    | {
    type: "custom",
    content: any
};
