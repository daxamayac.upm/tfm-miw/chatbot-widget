import {createNewBotChatMessage} from "app/models/ChatMessage";

export type RasaMessage =
    | {| sender_id: string, text: string |}
    | {|
    sender_id: string,
    buttons: Array<{ title: string, payload: string, selected?: boolean }>,
    text?: string
|}
    | {| sender_id: string, image: string, text?: string |}
    | {| sender_id: string, attachment: string, text?: string |}
    | {| sender_id: string, custom: string, text?: string |};



export function createNewRasaMessage(userId: string, messageText:string): RasaMessage {
    return {
        sender: userId,
        message: messageText
    };
}

export function rasaMessageToBotChatMessage(message: RasaMessage) {
    if (message.text) {
        return createNewBotChatMessage({type: "text", text: message.text});
    }
    if (message.buttons) {
        return createNewBotChatMessage({type: "button", buttons: message.buttons});
    }
    if (message.image) {
        return createNewBotChatMessage({type: "image", image: message.image});
    }
    // probably should be handled with special UI elements
    if (message.attachment) {
        return createNewBotChatMessage({type: "text", text: message.attachment});
    }
    return null;
}
