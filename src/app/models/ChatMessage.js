import type {MessageType} from "app/models/MessageType";
import {uuidv4} from "app/shared/utils";

export type ChatMessage = {
    message: MessageType,
    time: MessageType,
    username: string,
    uuid: string,
    voiceLang?: string
};


export function createNewBotChatMessage(botMessageObj: MessageType): ChatMessage {
    return {
        message: botMessageObj,
        time: Date.now(),
        username: "bot",
        uuid: uuidv4()
    };
}

export function createNewUserChatMessage(userId: string, messageText: string): ChatMessage {
    return {
        message: {type: "text", text: messageText},
        time: Date.now(),
        username: userId,
        uuid: uuidv4()
    };
}
