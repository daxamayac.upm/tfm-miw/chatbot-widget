/**
 * @jest-environment jsdom
 */
import React from "react";

import {render, unmountComponentAtNode} from "react-dom";
import {act} from "react-dom/test-utils";
import Message from "app/Message";

let container = null;
beforeEach(() => {
    // configurar un elemento del DOM como objetivo del renderizado
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    // limpieza al salir
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});


test("message text component ", () => {
    localStorage.setItem("notifyBy", "rasa")

    let message = {message: {type: "text", text: "mensajito de text"}, onButtonClick: false}
    act(() => {
        render(
            <Message
                chat={message}
                key={0}
                onButtonClick={true}
            />, container);

    });

    expect(container.querySelector("span").textContent).toEqual("mensajito de text");
});

test("message buttons component onButtonClick: true, ", () => {
    localStorage.setItem("notifyBy", "rasa")

    let message = {
        message: {
            type: "button",
            buttons: [{title: "yes", payload: "/trigger_yes"}, {title: "no", payload: "/trigger_no"}]
        }, onButtonClick: true
    }
    act(() => {
        render(
            <Message
                chat={message}
                key={0}
                onButtonClick={true}
            />, container);

    });

    expect(container.querySelector(".chat-button:nth-child(1) span").textContent).toEqual("yes");
    expect(container.querySelector(".chat-button:nth-child(2) span").textContent).toEqual("no");
});

test("message buttons component onButtonClick: false,", () => {
    localStorage.setItem("notifyBy", "rasa")

    let message = {
        message: {
            type: "button",
            buttons: [{title: "yes", payload: "/trigger_yes"}, {title: "no", payload: "/trigger_no"}]
        }
    }
    act(() => {
        render(
            <Message
                chat={message}
                key={0}
                onButtonClick={null}
            />, container);

    });

    expect(container.querySelector(".chat-button:nth-child(1) span").textContent).toEqual("yes");
    expect(container.querySelector(".chat-button:nth-child(2) span").textContent).toEqual("no");
});

test("message image component bot ", () => {
    localStorage.setItem("notifyBy", "rasa")

    let message = {
        message: {
            type: "image",
            image: "https://localhost:3000/image.svg"
        }, onButtonClick: true,
        username: "bot"
    }
    act(() => {
        render(
            <Message
                chat={message}
                key={0}
                onButtonClick={true}
            />, container);

    });
    expect(container.querySelector("img").src).toEqual("https://localhost:3000/image.svg");
});

test("message image component no bot", () => {
    localStorage.setItem("notifyBy", "rasa")

    let message = {
        message: {
            type: "image",
            image: "https://localhost:3000/image.svg"
        }, onButtonClick: true,
        username: "David"
    }
    act(() => {
        render(
            <Message
                chat={message}
                key={0}
                onButtonClick={true}
            />, container);

    });
expect(container.querySelector("img").src).toEqual("https://localhost:3000/image.svg");
});


test("message null component ", () => {
    localStorage.setItem("notifyBy", "rasa")

    let message = {
        message: {
            type: "asdasd",
        }, onButtonClick: true
    }
    act(() => {
        render(
            <Message
                chat={message}
                key={0}
                onButtonClick={true}
            />, container);

    });
    expect(container.textContent).toEqual("");
});
