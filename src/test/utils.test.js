import {uuidv4, sleep} from "app/shared/utils";

import "@babel/polyfill";

test('uuid4 format', () => {
    const data = uuidv4();
    const verifyNumber = data.split("-");
    expect(verifyNumber.length).toEqual(5);
    expect(verifyNumber[2].charAt(0)).toEqual("4");
});

test('sleep', async () => {
    await expect(sleep(500)).resolves.not.toThrow();

});
