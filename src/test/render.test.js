/**
 * @jest-environment jsdom
 */
import React from "react";

import {render, unmountComponentAtNode} from "react-dom";
import {act} from "react-dom/test-utils";
import {enableFetchMocks} from 'jest-fetch-mock'

import Chatroom from "app/Chatroom";
import ConnectedChatroom from "app/ConnectedChatroom";
import WS from "jest-websocket-mock";
import EventEmitter from "app/shared/EventEmitter";

enableFetchMocks()
let container = null;
let server: WS;

beforeEach(() => {
    // configurar un elemento del DOM como objetivo del renderizado
    container = document.createElement("div");
    document.body.appendChild(container);

    server = new WS("ws://localhost:1234", {jsonProtocol: true});
    server.on("message", (message) => {
        console.log("mensaje recibido")
    });

    jest.useFakeTimers();
});

afterEach(() => {
    // limpieza al salir
    unmountComponentAtNode(container);
    container.remove();
    container = null;
    WS.clean();
    jest.useRealTimers();

});

test("chat title", () => {
    act(() => {
        render(<Chatroom isOpen={true} messages={""} title={"VixBot"}/>, container);
    });

    expect(container.querySelector("h3").textContent).toBe("VixBot");

});

test("chat title", () => {
    act(() => {
        render(<Chatroom isOpen={false} messages={""} title={"VixBot"}/>, container);
    });

    expect(container.querySelector("h3").textContent).toBe("VixBot");

});

test("ConnectedChatroom channel rasa ", () => {
    localStorage.setItem("notifyBy", "rasa")
    fetch.mockResponseOnce(JSON.stringify([{sender_id: "seeder_id", text: "######texto del mensaje"}]));

    act(() => {
        render(<ConnectedChatroom host={''} welcomeMessage={'Hi, i am Vix'} title={'VixBot'}
                                  userId={''} handoffIntent={''} waitingTimeout={''}/>, container);
    });
    expect(container.querySelector("h3").textContent).toEqual("VixBot");
    expect(container.querySelector(".chats>.chat:nth-child(1) span").textContent).toEqual("Hi, i am Vix");
    expect(container.querySelector("form [type=text]").value).toEqual("");
    expect(container.querySelector("form [type=submit]").value).toEqual("Enviar");

    let inputText = container.querySelector("form [type=text]");
    let button = container.querySelector("form [type=submit]");

    inputText.value = "hola..";

    act(() => {
        button.dispatchEvent(new MouseEvent("click", {bubbles: true}));
        jest.advanceTimersByTime(8000);
    });

    expect(container.querySelector(".chats>.chat:nth-child(3) span").textContent).toEqual("hola..");

});


test("ConnectedChatroom alternative ", () => {
    localStorage.setItem("notifyBy", "rasa")
    fetch.mockResponseOnce(JSON.stringify([{sender_id: "seeder_id", text: "######texto del mensaje"}]));

    act(() => {
        render(<ConnectedChatroom host={''} title={'VixBot'}
                                  userId={''} handoffIntent={''} waitingTimeout={''}/>, container);
    });
    expect(container.querySelector("h3").textContent).toEqual("VixBot");
    expect(container.querySelector("form [type=text]").value).toEqual("");
    expect(container.querySelector("form [type=submit]").value).toEqual("Enviar");

    let inputText = container.querySelector("form [type=text]");
    let button = container.querySelector("form [type=submit]");

    inputText.value = "";

    act(() => {
        button.dispatchEvent(new MouseEvent("click", {bubbles: true}));
        jest.advanceTimersByTime(8000);
    });

    expect(container.querySelector(".chats").textContent).toEqual("");

});


test("ConnectedChatroom blacklist ", () => {
    localStorage.setItem("notifyBy", "rasa")
    fetch.mockResponseOnce(JSON.stringify([{sender_id: "seeder_id", text: "######texto del mensaje"}]));

    act(() => {
        render(<ConnectedChatroom host={''} title={'VixBot'}
                                  userId={''} handoffIntent={''} waitingTimeout={''}/>, container);
    });
    expect(container.querySelector("h3").textContent).toEqual("VixBot");
    expect(container.querySelector("form [type=text]").value).toEqual("");
    expect(container.querySelector("form [type=submit]").value).toEqual("Enviar");

    let inputText = container.querySelector("form [type=text]");
    let button = container.querySelector("form [type=submit]");

    inputText.value = "/handoff";

    act(() => {
        button.dispatchEvent(new MouseEvent("click", {bubbles: true}));
        jest.advanceTimersByTime(8000);
    });

    expect(container.querySelector(".chats").textContent).toEqual("");

});

test("ConnectedChatroom channel slack emit(slackNewMessage)", () => {
    localStorage.setItem("notifyBy", "slack")
    fetch.mockResponseOnce(JSON.stringify([{sender_id: "seeder_id", text: "######texto del mensaje"}]));

    act(() => {
        render(<ConnectedChatroom host={''} welcomeMessage={'Hi, i am Vix'} title={'VixBot'}
                                  userId={''} handoffIntent={''} waitingTimeout={''}/>, container);
    });

    EventEmitter.emit("newSlackMessage", {text: "slack message"})
    jest.advanceTimersByTime(8000);

    expect(container.querySelector(".chats>.chat:nth-child(2) span").textContent).toEqual("slack message");
});
