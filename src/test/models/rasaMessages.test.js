import {RasaMessage, createNewRasaMessage, rasaMessageToBotChatMessage} from "app/models/RasaMessage";
import {createNewBotChatMessage, createNewUserChatMessage} from "app/models/ChatMessage";
import type {MessageType} from "app/models/MessageType";

test('createNewRasaMessage', () => {
    const data = createNewRasaMessage('894855', 'mensajito');
    expect(data).toEqual({sender: '894855', message: 'mensajito'});
});

const rasaMessage: RasaMessage = {sender_id: 'td4asd32das3das4sd3', text: '123'}
const botMessageObject: MessageType = {type: 'text', text: '123'}



test('createNewUserChatMessage', () => {
    const data = createNewUserChatMessage('894855', 'mensajito');
    expect(data)
        .toMatchObject({
            message: {type: "text", text: 'mensajito'},
            username: '894855'
        });
});


test('createNewBotChatMessage', () => {
    const data = createNewBotChatMessage(botMessageObject);
    expect(data)
        .toMatchObject({
            message: botMessageObject,
            username: 'bot'
        });
});



test('rasaMessageToBotChatMessage', () => {
    const data = rasaMessageToBotChatMessage(rasaMessage);
    expect(data)
        .toMatchObject({
            message: botMessageObject,
            username: 'bot'
        });
});

const rasaUndefinedMessage: RasaMessage = {sender_id: 'td4asd32das3das4sd3', undefined: '123'}
test('rasaMessageToBotChatMessageUndefined', () => {
    const data = rasaMessageToBotChatMessage(rasaUndefinedMessage);
    expect(data).toBeNull();
});

const rasaButtonMessage: RasaMessage = {sender_id: 'td4asd32das3das4sd3', buttons: '123'}
test('rasaMessageToBotChatMessageButton', () => {
    const data = rasaMessageToBotChatMessage(rasaButtonMessage);
    expect(data).toMatchObject({
        message: {type:'button'},
        username: 'bot'
    });
});

const rasaImageMessage: RasaMessage = {sender_id: 'td4asd32das3das4sd3', image: '123'}
test('rasaMessageToBotChatMessageImage', () => {
    const data = rasaMessageToBotChatMessage(rasaImageMessage);
    expect(data).toMatchObject({
        message: {type:'image'},
        username: 'bot'
    });
});

const rasaAttachmentMessage: RasaMessage = {sender_id: 'td4asd32das3das4sd3', attachment: '123'}
test('rasaMessageToBotChatMessageAttachment', () => {
    const data = rasaMessageToBotChatMessage(rasaAttachmentMessage);
    expect(data).toMatchObject({
        message: {type:'text'},
        username: 'bot'
    });
});

