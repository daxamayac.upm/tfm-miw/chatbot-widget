import {createNewSlackMessage, slackMessageToBotChatMessage} from "app/models/SlackMessage";
import type {SlackMessage} from "app/models/SlackMessage";

test('createNewSlackMessage', () => {
    const data = createNewSlackMessage('894855', 'mensajito', 'alex');
    expect(data).toEqual({channel: '894855', text: 'mensajito', username: 'alex'});
});

test('createNewSlackMessageCustomUsername', () => {
    const data = createNewSlackMessage('894855', 'mensajito');
    expect(data).toEqual({channel: '894855', text: 'mensajito', username: 'Vixi'});
});

const slackMessage: SlackMessage = {ts: '1200', text: '123', user: 'asda', channel: 'channel1'}
test('slackMessageToBotChatMessage', () => {
    const data = slackMessageToBotChatMessage(slackMessage);
    expect(data)
        .toMatchObject({
            message: {type: 'text', text: '123'},
            username: 'bot'
        });
});


test('slackMessageToBotChatMessageNUll', () => {
    const data = slackMessageToBotChatMessage("asd");
    expect(data).toEqual(null);
});
