/**
 * @jest-environment jsdom
 */

import "@babel/polyfill";
import {RasaChannel} from "app/services/channel/RasaChannel";
import {enableFetchMocks} from 'jest-fetch-mock'

enableFetchMocks()

test("endSlackHandoff", () => {
    let rasaChannel = new RasaChannel();
    sessionStorage.setItem("simple-chatroom-cid", "seeder")
    const data = rasaChannel._getUserId();
    expect(data).toEqual("seeder");
});


test("sendMessage", async () => {
    fetch.mockResponseOnce(JSON.stringify({a: "asd"}));
    let rasaChannel = new RasaChannel();
    await rasaChannel.sendMessage("hola");

});
