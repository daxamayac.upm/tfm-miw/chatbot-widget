/**
 * @jest-environment jsdom
 */

import "@babel/polyfill";
import {RasaChannel} from "app/services/channel/RasaChannel";
import {enableFetchMocks} from 'jest-fetch-mock'
import {ChannelFactory} from "app/services/channel/ChannelFactory";
import {ChannelType} from "app/models/ChannelType";
import {SlackChannel} from "app/services/channel/SlackChannel";
import WS from "jest-websocket-mock";

enableFetchMocks()

let server: WS;
beforeEach(async () => {
    server = new WS("ws://localhost:1234", {jsonProtocol: true});
    server.on("message", (message) => {
        console.log("mensaje recibido")
    });

});

afterEach(() => {
    WS.clean();
});



test("factory rasa channel", () => {
    let channelFactory = new ChannelFactory();
    let channel = channelFactory.get(ChannelType.RASA)

    expect(channel).toBeInstanceOf(RasaChannel)
});

test("factory slack channel", async() => {
    fetch.mockResponseOnce(JSON.stringify([{
        "recipient_id": "df81951d-37b4-4dd4-a60d-3b01faa108ff",
        "custom": {"wss": "ws://localhost:1234"}
    }]));
    let channelFactory = new ChannelFactory();
    let channel = channelFactory.get(ChannelType.SLACK)
    await server.connected;
    expect(channel).toBeInstanceOf(SlackChannel)
});

test("factory null channel", () => {
    let channelFactory = new ChannelFactory();
    let channel = channelFactory.get("null")

    expect(channel).toEqual(null)
});
