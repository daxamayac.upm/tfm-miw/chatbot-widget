/**
 * @jest-environment jsdom
 */
import React from "react";
import "@babel/polyfill";
import notificationService from "app/services/NotificationService";
import {unmountComponentAtNode} from "react-dom";
import {ChannelType} from "app/models/ChannelType";


let container = null;
beforeEach(() => {
    // configurar un elemento del DOM como objetivo del renderizado
    container = document.createElement("div");
    document.body.appendChild(container);
    jest.useFakeTimers();
});

afterEach(() => {
    // limpieza al salir
    unmountComponentAtNode(container);
    container.remove();
    container = null;
    jest.useRealTimers();
});

test("startSlackHandoff", () => {
    const data = {
        channel: "channel-id",
        token: "clavesita"
    }

    notificationService.startSlackHandoff(data)

    let token = localStorage.getItem("token")
    let channel = localStorage.getItem("channel")
    let notifyBy = notificationService._getNotifyBy()

    expect(channel).toEqual(data.channel)
    expect(token).toEqual(data.token)
    expect(notifyBy).toEqual(ChannelType.SLACK)
});

test("endSlackHandoff", () => {

    notificationService.endSlackHandoff()

    let notifyBy = notificationService._getNotifyBy()
    let token = localStorage.getItem("token")
    let channel = localStorage.getItem("channel")

    expect(channel).toEqual(null)
    expect(token).toEqual(null)
    expect(notifyBy).toEqual(ChannelType.RASA)
});

