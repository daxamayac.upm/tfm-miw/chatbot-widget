/**
 * @jest-environment jsdom
 */

import "@babel/polyfill";


import WS from "jest-websocket-mock";
import {SlackChannel} from "app/services/channel/SlackChannel";
import {enableFetchMocks} from 'jest-fetch-mock'

const slackMessage = {"envelope_id":"967cf0e4-1d97-4746-bdd1-0f42785f997d","payload":{"token":"o4NyzwI7DvT8wFs8okEwxnlI","team_id":"T02384XE742","api_app_id":"A022TFGEZ4P","event":{"client_msg_id":"c56507c9-513f-486a-bf3e-570d64093be2","type":"message","text":"soy asesor","user":"U023LP1KT5X","ts":"1622898623.002100","team":"T02384XE742","blocks":[{"type":"rich_text","block_id":"Q7M7","elements":[{"type":"rich_text_section","elements":[{"type":"text","text":"soy asesor"}]}]}],"channel":"C0240C79LKT","event_ts":"1622898623.002100","channel_type":"channel"},"type":"event_callback","event_id":"Ev0244JGJULT","event_time":1622898623,"authorizations":[{"enterprise_id":null,"team_id":"T02384XE742","user_id":"U0231LFBW6S","is_bot":true,"is_enterprise_install":false}],"is_ext_shared_channel":false,"event_context":"2-message-T02384XE742-A022TFGEZ4P-C0240C79LKT"},"type":"events_api","accepts_response_payload":false,"retry_attempt":0,"retry_reason":""}

enableFetchMocks()
describe("Testing Jest Websocket Mock", () => {
    let server: WS;
    beforeEach(async () => {
        server = new WS("ws://localhost:1234", {jsonProtocol: true});
        server.on("message", (message) => {
            console.log("mensaje recibido")
        });

    });

    afterEach(() => {
        WS.clean();
    });

    test("advisor send message to widget", async () => {

        fetch.mockResponseOnce(JSON.stringify([{
            "recipient_id": "df81951d-37b4-4dd4-a60d-3b01faa108ff",
            "custom": {"wss": "ws://localhost:1234"}
        }]));
        new SlackChannel()
        await server.connected;
        server.send(slackMessage)

        await expect(server).toReceiveMessage({"envelope_id": "967cf0e4-1d97-4746-bdd1-0f42785f997d"});

    });

});
